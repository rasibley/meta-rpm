#!/bin/bash

if [ ! -d /data/meta-rpm -o ! -d /data/build ]; then
    echo The build container not setup, to do so run:
    echo $ exec initial-setup
else
    . /data/meta-rpm/oe-init-build-env /data/build
fi

if [ "$#" != 0 ]; then
    exec bash -c "$*"
fi
exec bash
