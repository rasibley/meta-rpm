#!/bin/bash

cd /data
echo ==== Cloning meta-rpm repo in /data/meta-rpm ====
git clone --recurse-submodules https://gitlab.com/fedora-iot/meta-rpm.git

echo ==== Setting up build environment in /data/build ====
. /data/meta-rpm/oe-init-build-env /data/build
exec bash
