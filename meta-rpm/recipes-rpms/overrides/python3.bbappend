post_rpm_install_append_class-native() {
  mkdir ${D}/${bindir}/python3-native

  for lnk in ${D}/${bindir}/python3.6*; do
      LNK=$(readlink $lnk)
      BASENAME=$(basename $lnk)
      DIRNAME=$(dirname $lnk)
      ln -s ../$LNK $DIRNAME/python3-native/$BASENAME
      rm $lnk
  done
  ln -s python3-native/python3 ${D}/${bindir}/nativepython3
  # This is an alternative in the rpm, just make it a symlink
  ln -s python3.6 ${D}/${bindir}/python3-native/python3
}
