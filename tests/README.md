# Tests for meta-rpm

Includes a list of tests to be run against each Merge Request for
meta-rpm repo to ensure proposed changes didn't break our build.

These tests are written in Ansible, and will run the following checks:
 * Install Yocto Dependencies in AWS to prepare the build
 * Build the sample images for meta-rpm
 * Verify the contents of the image using bwrap
 * More tests to be added soon !
